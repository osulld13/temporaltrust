<?php

include_once "TrustFactor.php";

class ActivityFactor extends TrustFactor
{
	function calculate($frequency_constant)
	{
		$m = $this->basefactors->Cardinality($this->basefactors->IntervalAgentOutputInteractionsFunction($this->agent, $this->basefactors->system->time_0, $this->time)) 
			+ $this->basefactors->Cardinality($this->basefactors->IntervalAgentInputInteractionsFunction($this->agent, $this->basefactors->system->time_0, $this->time));

		$result = $m / $this->basefactors->Cardinality($this->basefactors->SystemInteractionsFunction($this->time));
		return $result;
	}
}

?>