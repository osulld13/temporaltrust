<?php
ini_set('max_execution_time', 0);
include 'FrequencyFactor.php';
include 'PresenceFactor.php';
include 'RegularityFactor.php';
include 'ActivityFactor.php';

$conn = new mysqli("localhost", "root", "", "temporaltrust");
//mysqli_query($conn,"DROP TABLE tempfactors");


mysqli_query($conn, "CREATE TABLE IF NOT EXISTS tempfactors (AgentId int,  ActivityFactor double, FrequencyFactor double, PresenceFactor double, RegularityFactor double, Frequency int, Dataset varchar(50), Reputation int, primary key (AgentId,Frequency))");


// Change the name of the dataset here to calculate for different datasets.
$table = "bicycles";



$time = "2012-08-25T18:10:18.440";
$day = 60 * 60 * 24;
$freq_const = array($day*1,$day*3,$day*7,$day*31);
$base = new BaseFunction($table);
//$factor = new ActivityFactor(7,"2014-09-05T16:41:59.513", $base);

//$reg = new ActivityFactor(2000, $time, $base);
//echo "<p>".$reg->calculate($freq_const[1])."</p>";

// Uncomment this section and the fputcsv lower down to store results in a json file for easier graphing.
/*$fp = fopen('../FrontEndStuff/temporalfactors.json', 'w');
$array = array('name','temporalfactor1','temporalfactor2','temporalfactor3','temporalfactor4');
fputcsv($fp, $array);*/

foreach($base->system->agent_set as $curr_agent){
	$array = array();
	$index = 0;
	$array[$index++] = $curr_agent["Name"];
	foreach($freq_const as $curr_freq){
		$freq = new FrequencyFactor($curr_agent["EntityId"], $time, $base);
		$pres = new PresenceFactor($curr_agent["EntityId"], $time, $base);
		$act = new ActivityFactor($curr_agent["EntityId"], $time, $base);
		$reg = new RegularityFactor($curr_agent["EntityId"], $time, $base);
		$freqc = $freq->calculate($curr_freq);
		$presc = $pres->calculate($curr_freq);
		$actc = $act->calculate($curr_freq);
		$regc = $reg->calculate($curr_freq);

		$tot = ($freqc/4) + ($presc/4) + ($actc/4) + ($regc/4);
		$array[$index++] = $tot;
		$query = "INSERT INTO tempfactors VALUES('".$curr_agent["EntityId"]."','".$actc."','".$freqc."','".$presc."','".$regc."','".$curr_freq."','".$table."','".$curr_agent["Reputation"]."')";
        echo $query;
        mysqli_query($conn, $query);
	}
//	fputcsv($fp, $array);
}



?>