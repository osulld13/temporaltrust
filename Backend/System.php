<?php
include "Tables.php";


class LTTM_L
{
	public $time_0;
	public $agent_set;
	// public $time_domain;
	public $interactions_table;
//	public $frequency_constant;

	public $dataset;


	function __construct($_dataset)
	{
		$this->dataset = $_dataset;
		$this->LoadAgents();
		$this->LoadInteractions();
		$this->frequency_constant = 4;
	}

	function LoadAgents()
	{
		$table = new Tables();
		$this->agent_set = $table->CreateTable("entity",$this->dataset);

		foreach($this->agent_set as $temp)
		{
			if($temp["EntityId"] == -1){
				$this->time_0 = $temp["Time"];
				break;
			}
		}
	}

	function LoadInteractions()
	{
		$table = new Tables();
		$temp = $table->CreateTable("interaction",$this->dataset);
		$this->interactions_table = array();

		$index = 0;
		foreach($temp as $row)
		{
			$tuple = array("AlphaId"=>$row["AlphaId"], "Time"=>$row["Time"], "BetaId"=>$row["BetaID"]);
			$this->interactions_table[$index++] = $tuple;
		}
	}
}


?>