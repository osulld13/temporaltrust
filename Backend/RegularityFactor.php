<?php

include_once "TrustFactor.php";

class RegularityFactor extends TrustFactor
{
	function calculate($frequency_constant)
	{
		if($this->time < $this->basefactors->system->time_0){
			return 0;
		}

		$max = $this->basefactors->AgentLifeCycleFunction($this->agent, $this->time) / $frequency_constant;
		$sum = 0;


		$n = $this->basefactors->AgentBornFunction($this->agent);


		for($i = 0; $i < $max; $i++)
		{
			$temp = $this->basefactors->PresenceFunction($this->agent, $n + $i*$frequency_constant, $n + $i*$frequency_constant + $frequency_constant);
			$temp = $temp / $max;
			$sum += $temp;
		}

		return $sum;
	}
}

?>