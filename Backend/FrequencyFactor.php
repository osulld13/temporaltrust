<?php

include_once "TrustFactor.php";

class FrequencyFactor extends TrustFactor
{
	function calculate($frequency_constant)
	{
		$m = $this->basefactors->Cardinality($this->basefactors->IntervalAgentOutputInteractionsFunction($this->agent, $this->basefactors->system->time_0, $this->time)) 
			+ $this->basefactors->Cardinality($this->basefactors->IntervalAgentInputInteractionsFunction($this->agent, $this->basefactors->system->time_0, $this->time));
		echo $m;
		$m = $m / $this->basefactors->AgentLifeCycleFunction($this->agent, $this->time);

		//$m = $m * $this->basefactors->system->$frequency_constant;
		$m = $m * $frequency_constant;

		if($m > 1)
		{
			$m = 1;
		}
		return $m;
	}
}

?>