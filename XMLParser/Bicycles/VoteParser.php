<?php

class Vote{
	
	public $id;
	public $postId;
	public $voteTypeId;
	public $creationDate;
	
	function __construct($idIn, $postIdIn, $voteTypeIdIn, $creationDateIn){
		$this -> id = $idIn;
		$this -> postId = $postIdIn;
		$this -> voteType = $voteTypeIdIn;
		$this -> creationDate = $creationDateIn;
	}
	
	public function getId(){
		return $this -> id;
	}

	public function getPostId(){
		return $this -> postId;
	}
	
	public function getVoteTypeId(){
		return $this -> voteTypeId;
	}

	public function getCreationDate(){
		return $this -> creationDate;
	}
	
	public function echoVote(){
		echo "Id = ", $this -> id," Post Id = ", $this -> postId," Vote Type Id = ", 
		$this -> voteTypeId," Creation Date = ",
		$this -> creationDate, "<br>";
	}
	
}


function VoteParser(){
	$xml = simplexml_load_file("Votes.xml");
    $Votes = array();
	foreach($xml->row as $a){
		$tempId;
		$tempPostId;
		$tempVoteTypeId;
		$tempCreationDate;
		foreach($a->attributes() as $b => $c) {
			if($b == "Id"){
				$tempId = $c;
			}
		    elseif($b == "PostId"){
				$tempPostId = $c;
			}
		    elseif($b == "VoteTypeId"){
				$tempVoteTypeId = $c;
			}
		    elseif($b == "CreationDate"){
				$tempCreationDate = $c;
			}
		}
		$Vote = new Vote($tempId, $tempPostId, $tempVoteTypeId, $tempCreationDate);
		array_push($Votes, $Vote);
	}
	return $Votes;
}

$VoteArray = VoteParser();

//foreach($VoteArray as $b){
//	$b -> echoVote();
//}

?>