<?php
class Post{
	
	public $id;
	public $postTypeId;
	public $acceptedAnswerId;
	public $creationDate;
	public $score;
	public $viewCount;
	public $body;
	public $ownerUserId;
	public $lastActivityDate;
	public $title;
	public $tags;
	public $answerCount;
	public $favoriteCount;
	
	function __construct($idIn, $postTypeIdIn, $acceptedAnswerIdIn,
	$creationDateIn, $scoreIn, $viewCountIn, $BodyIn, $ownerUserIdIn,
	 $lastActivityDateIn, $titleIn, $tagsIn, $answerCountIn, $favoriteCountIn){
		$this -> id = $idIn;
		$this -> postTypeId = $postTypeIdIn;
		$this -> acceptedAnswerId = $acceptedAnswerIdIn;
		$this -> creationDate = $creationDateIn;
		$this -> score = $scoreIn;
		$this -> viewCount = $viewCountIn;
		$this -> body = $BodyIn;
		$this -> ownerUserId = $ownerUserIdIn;
		$this -> lastActivityDate = $lastActivityDateIn;
		$this -> title = $titleIn;
		$this -> tags = $tagsIn;
		$this -> answerCount = $answerCountIn;
		$this -> favoriteCount = $favoriteCountIn;
	}
	
	public function getId(){
		return $this -> id;
	}

    public function getPostTypeId(){
		return $this -> postTypeId;
	}	

	public function getAcceptedAnswerId(){
		return $this -> acceptedAnswerId;
	}	
	
	public function getCreationDate(){
		return $this -> creationDate;
	}
	
	public function getScore(){
		return $this -> score;
	}

    public function getViewCount(){
		return $this -> viewCount;
	}	
	
	public function getBody(){
		return $this -> body;
	}

	public function getOwnerUserId(){
		return $this -> ownerUserId;
	}

	public function getLastActivityDate(){
		return $this -> lastActivityDate;
	}

	public function getTitle(){
		return $this -> title;
	}

	public function getTags(){
		return $this -> tags;
	}

	public function getAnswerCount(){
		return $this -> answerCount;
	}

	public function getFavoriteCount(){
		return $this -> favoriteCount;
	}
	
	public function echoPost(){
		echo "Id = ", $this -> getId()," Post Type Id = ", $this -> getPostTypeId()," Accepted Answer Id = ", $this -> getAcceptedAnswerId(),
		" Creation Date = ", $this -> getCreationDate(), " Score = ", $this -> getScore(), " View Count = ", $this -> getViewCount(), " Body = ", $this -> getBody(),
		" Owner User Id = ", $this -> getOwnerUserId(), " Last Activity Date = ", $this -> getLastActivityDate(), " Title = ", $this -> getTitle(),
		 " Tags = ", $this -> getTags(), " AnswerCount = ", $this -> getAnswerCount(), " Favorite Count = ", $this -> getFavoriteCount(), "<br>";
	}
	
}


function PostParser(){
	$xml = simplexml_load_file("Posts.xml");
    $Posts = array();
	foreach($xml->row as $a){
		$tempId;
		$tempPostTypeId;
		$tempAcceptedAnswerId;
		$tempCreationDate;
		$tempScore;
		$tempViewCount;
		$tempBody;
		$tempOwnerUserId;
		$tempLastActivityDate;
		$tempTitle;
		$tempTags;
		$tempAnswerCount;
		$tempFavoriteCount;
		foreach($a->attributes() as $b => $c) {
			if($b == "Id"){
				$tempId = $c;
			}
			elseif($b == "PostTypeId"){
				$tempPostTypeId = $c;
			}
		    elseif($b == "AcceptedAnswerId"){
				$tempAcceptedAnswerId = $c;
			}
			elseif($b == "CreationDate"){
				$tempCreationDate = $c;
			}
			elseif($b == "Score"){
				$tempScore = $c;
			}
		    elseif($b == "ViewCount"){
				$tempViewCount = $c;
			}
		    elseif($b == "Body"){
				$tempBody = $c;
			}
			elseif($b == "OwnerUserId"){
				$tempOwnerUserId = $c;
			}
			elseif($b == "LastActivityDate"){
				$tempLastActivityDate = $c;
			}
			elseif($b == "Title"){
				$tempTitle = $c;
			}
			elseif($b == "Tags"){
				$tempTags = $c;
			}
			elseif($b == "AnswerCount"){
				$tempAnswerCount = $c;
			}
			elseif($b == "FavoriteCount"){
				$tempFavoriteCount = $c;
			}
		}
		$Post = new Post($tempId, $tempPostTypeId, $tempAcceptedAnswerId,$tempCreationDate, $tempScore, 
			$tempViewCount, $tempBody, $tempOwnerUserId, $tempLastActivityDate, $tempTitle, $tempTags, 
			$tempAnswerCount, $tempFavoriteCount);
		array_push($Posts, $Post);
	}
	return $Posts;
}

$PostArray = PostParser();
 
?>