<?php

class User{
	
	public $id;
	public $reputation;
    public $creationDate;
	public $displayName;
	public $lastAccessDate;
    public $location;
    public $aboutMe;
    public $views;
    public $upVotes;
    public $downVotes;
    public $emailHash;
	
	function __construct($idIn, $reputationIn, $creationDateIn, $displayNameIn, $lastAccessDateIn
		, $locationIn, $aboutMeIn, $viewsIn, $upVotesIn, $downVotesIn, $emailHashIn){
		$this -> id = $idIn;
		$this -> reputation = $reputationIn;
		$this -> creationDate = $creationDateIn;
		$this -> displayName = $displayNameIn;
  		$this -> lastAccessDate = $lastAccessDateIn;
  		$this -> location = $locationIn;
  		$this -> aboutMe = $aboutMeIn;
  		$this -> views = $viewsIn;
  		$this -> upVotes = $upVotesIn;
  		$this -> downVotes = $downVotesIn;
  		$this -> emailHash = $emailHashIn;
	}
	
	public function getId(){
		return $this -> id;
	}

	public function getReputation(){
		return $this -> reputation;
	}
	
	public function getCreationDate(){
		return $this -> creationDate;
	}

	public function getDisplayName(){
		return $this -> displayName;
	}

	public function getLastAccessDate(){
		return $this -> lastAccessDate;
	}

	public function getLocation(){
		return $this -> location;
	}

	public function getAboutMe(){
		return $this -> aboutMe;
	}

	public function getViews(){
		return $this -> views;
	}

	public function getUpVotes(){
		return $this -> upVotes;
	}

	public function getDownVotes(){
		return $this -> downVotes;
	}

	public function getEmailHash(){
		return $this -> emailHash;
	}
	
	public function echoUser(){
		echo "Id = ", $this -> id," Reputation = ", $this -> reputation," Creation Date = ", $this -> creationDate," Display Name = ",
		$this -> displayName, " Last Access Date = ", $this -> lastAccessDate," Location = ", $this -> location," About Me = ", 
		$this -> aboutMe," Views = ", $this -> views," Up Votes = ", $this -> upVotes," Down Votes = ", $this -> downVotes,
		" Email Hash = ", $this -> emailHash,"<br>";
	}
	
}


function userParser(){
	$xml = simplexml_load_file("Users.xml");
    	$users = array();
	foreach($xml->row as $a){
		$tempId;
		$tempReputation;
		$tempCreationDate;
		$tempDisplayName;
		$tempLastAccessDate;
		$tempLocation;
		$tempAboutMe;
		$tempViews;
		$tempUpVotes;
		$tempDownVotes;
		$tempEmailHash;
		foreach($a->attributes() as $b => $c) {
			if($b == "Id"){
				$tempId = $c;
			}
		    elseif($b == "Reputation"){
				$tempReputation = $c;
			}
		    elseif($b == "CreationDate"){
				$tempCreationDate = $c;
			}
		    elseif($b == "DisplayName"){
				$tempDisplayName = $c;
			}
		    elseif($b == "LastAccessDate"){
				$tempLastAccessDate = $c;
			}
		    elseif($b == "Location"){
				$tempLocation = $c;
			}
		    elseif($b == "AboutMe"){
				$tempAboutMe = $c;
			}
		    elseif($b == "Views"){
				$tempViews = $c;
			}
		    elseif($b == "UpVotes"){
				$tempUpVotes = $c;
			}
		    elseif($b == "DownVotes"){
				$tempDownVotes = $c;
			}
		    elseif($b == "EmailHash"){
				$tempEmailHash = $c;
			}			
		}
		$user = new User($tempId, $tempReputation, $tempCreationDate, $tempDisplayName
			, $tempLastAccessDate, $tempLocation, $tempAboutMe, 
			$tempViews, $tempUpVotes, $tempDownVotes, $tempEmailHash);
		array_push($users, $user);
	}
	return $users;
}

$userArray = userParser();

?>
